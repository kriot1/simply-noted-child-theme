<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Simply Noted Theme' );
define( 'CHILD_THEME_URL', 'http://www.riotcustoms.com/' );
define( 'CHILD_THEME_VERSION', '1.0.0' );

//* Force full-width-content layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'simply_noted_enqueue_scripts' );
function simply_noted_enqueue_scripts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700italic|Raleway:400,700|Lato:400,400italic,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_script( 'responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );
	wp_enqueue_script( 'sticky-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/sticky-nav.js', array( 'jquery' ), '1.0.0' );
	wp_enqueue_style( 'dashicons' );
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( 'headings', 'drop-down-menu',  'search-form', 'simply_notedip-links', 'rems' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'width'           => 500,
	'height'          => 150,
	'header-selector' => '.site-title > a',
	'header-text'     => false,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Remove support for 3-column footer widgets
remove_theme_support( 'genesis-footer-widgets', 3 );

// Register new image size for featured images
add_image_size( 'featured', 1024, 576, true );

//* Add support for after entry widget area
add_theme_support( 'genesis-after-entry-widget-area' );

//* Repositing Navigations
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_before_footer', 'genesis_do_subnav' );

//* Unregister layout settings
genesis_unregister_layout( 'content-sidebar' );
genesis_unregister_layout( 'sidebar-content' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

//* Unregister sidebar
unregister_sidebar( 'sidebar' );
unregister_sidebar( 'sidebar-alt' );
unregister_sidebar( 'header-right' );

// Register a custom shortcode that displays gravatar of post's author
add_shortcode( 'show_gravatar', 'func_show_gravatar' );
function func_show_gravatar() {
	return get_avatar( get_the_author_meta( 'ID' ), 32 );
}
// Customize entry meta in the entry header
add_filter( 'genesis_post_info', 'simply_noted_post_info_filter' );
function simply_noted_post_info_filter( $post_info ) {
	$post_info = '[show_gravatar] Written by [post_author_posts_link] on [post_date] [post_edit]';
	return $post_info;
}

//* Customize the entry meta in the entry footer
add_filter( 'genesis_post_meta', 'simply_noted_entry_meta_footer' );
function simply_noted_entry_meta_footer($post_meta) {
	$post_meta = '[post_categories before=""] [post_comments]';
	return $post_meta;
}

add_action( 'pre_get_posts', 'simply_noted_change_category_posts_per_page' );
/**
 * Change Posts Per Page for Category Archives
 *
 * @author Bill Erickson
 * @link http://www.billerickson.net/customize-the-wordpress-query/
 * @param object $query data
 *
 */
function simply_noted_change_category_posts_per_page( $query ) {

	if( $query->is_main_query() && !is_admin() && is_category() ) {
		$query->set( 'posts_per_page', '7' );
	}

}
//* Hook social widget area before site footer
add_action( 'genesis_footer', 'simply_noted_social_footer_widget_area', 7 );
function simply_noted_social_footer_widget_area() {
 
	genesis_widget_area( 'social-footer', array(
		'before' => '<div class="social-footer">',
		'after'  => '</div>',
	) );
 
}

// Register widget areas
genesis_register_sidebar( array(
	'id'          => 'social-footer',
	'name'        => __( 'Social Footer', 'simply noted' ),
	'description' => __( 'This is the social footer widget area.', 'simply_noted' ),
) );
