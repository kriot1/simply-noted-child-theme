<?php
/**
* This file lays out the Category Archive pages similar to No Sidebar Pro Theme's Front Page.
*/
// Force full width content
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
// Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
// Add featured image above the entry content
add_action( 'genesis_entry_header', 'ns_front_featured_image', 4 );
function ns_front_featured_image() {
	if ( $image = genesis_get_image( array( 'format' => 'url', 'size' => 'ns-featured', ) ) ) {
		printf( '<a class="ns-featured-image" href="' . get_permalink() . '" style="background-image: url(%s)"></a>', $image );
	}
}
// Add author gravatar before post meta
add_action( 'genesis_entry_header', 'ns_entry_gravatar' , 11 );
function ns_entry_gravatar() {
	echo get_avatar( get_the_author_meta( 'email' ), 96 );
}
// Customize entry meta in the entry header
add_filter( 'genesis_post_info', 'ns_entry_meta_header' );
function ns_entry_meta_header( $post_info ) {
	$post_info = '<span class="by">by</span> [post_author_posts_link] &middot; [post_date format="M j, Y"] [post_edit]';
	return $post_info;
}
// Remove entry content elements
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
remove_action( 'genesis_entry_content', 'genesis_do_post_content_nav', 12 );
remove_action( 'genesis_entry_content', 'genesis_do_post_permalink', 14 );
// Remove the entry meta in the entry footer
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
// Add post classes
add_filter( 'post_class', 'ns_post_class' );
function ns_post_class( $classes ) {
	global $wp_query;
	$current_page = is_paged() ? get_query_var('paged') : 1;
	$post_counter = $wp_query->current_post;
	if ( 0 == $post_counter && 1 == $current_page ) {
		$classes[] = 'first-featured';
	}
	if ( ( $post_counter & 1 ) && 1 == $current_page ) {
		$classes[] = 'row';
	} elseif ( ( $post_counter % 2 == 0 ) && 1 !== $current_page ) {
		$classes[] = 'row';
	}
	if ( ( $wp_query->current_post + 1 ) == $wp_query->post_count ) {
		$classes[] = 'last';
	}
	return $classes;
}
// Add first-page body class
add_filter( 'body_class', 'ns_body_class' );
function ns_body_class( $classes ) {
	$current_page = is_paged() ? get_query_var('paged') : 1;
	if ( 1 == $current_page ) {
		$classes[] = 'first-page';
	}
	$classes[] = 'category-page';
	return $classes;
}
// Run the Genesis function
genesis();