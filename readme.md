# Simply Noted Theme

BitBucket project link: https://bitbucket.org/kriot1/simply-noted-child-theme


## Credits and Thank You's
Brian Gardner, Bill Erickson, Sridhar Katakam and minimography.com 

## Installation Instructions

1. Make sure the Genesis Framework has been installed
2. Go to your WordPress dashboard and select Appearance and upload the Simply Noted theme.
4. Activate the Simply Noted theme.
5. Inside your WordPress dashboard, go to Customizations and configure them to your liking.


## Theme Support

This theme will not be activally support but please report any bugs to https://bitbucket.org/kriot1/simply-noted-child-theme/issues